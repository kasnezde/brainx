#!/usr/bin/env python3

""" Main program logic """

__author__ = "Zdeněk Kasner"

from argparse import ArgumentParser
from errors import *

import os
import sys
import imghdr
import encoder
import interpreter
import iohandler
import logger
import memory
import utils


def main():
    # set program location as current directory
    # os.chdir(sys.path[0])

    parser = ArgumentParser()

    # set program options
    parser.add_argument("-v", "--version", action="version", version="0.1")
    parser.add_argument("-m", "--memory", default = bytearray(b"\x00"), help="set default memory state", type = utils.string_to_bytes)
    parser.add_argument("-p", "--memory-pointer", default = 0, help="set default memory pointer position", type = int)
    parser.add_argument("-t", "--test", action="store_true", help="save debug information")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("--lc2f", action = "store_true", help="translate image to file")
    group.add_argument("--f2lc", action = "store_true", help="translate file [and image] to image")
    parser.add_argument("-i", nargs="*", help="input file(s) for f2lc translation")
    parser.add_argument("-o", help="output file for f2lc translation")


    parser.add_argument("--pnm", "--pbm", action = "store_true", help="translate input to PBM format")
    parser.add_argument("file", nargs="*", default = None, help="file with code to interpret")


    # parse command line arguments
    args = parser.parse_args()
     

    png_data = None     # image data input
    data = None         # program input

    # load program from console
    if not args.file and not args.i:
        instructions, data = iohandler.load_from_console()

    # load program from image (test with imghdr module)
    elif args.file and os.path.isfile(args.file[0]) and imghdr.what(args.file[0]):
        instructions, png_data = iohandler.load_png(args.file[0])

    # load program for translation from file / from argument
    elif args.i and len(args.i) == 1:
        instructions, data = iohandler.load_file(args.i[0])

    # load program and png data for translation from file
    elif args.i and len(args.i) == 2:
        instructions, data = iohandler.load_file(args.i[0])
        png_data = iohandler.load_png(args.i[1], False)

    # load program from file
    else:
        instructions, data = iohandler.load_file(args.file[0])


    # create new memory (default is empty)
    mem = memory.Memory(args.memory, args.memory_pointer)




    # save input png to pnm
    if args.pnm and png_data:
        if args.file and args.file[0]:
            pnm_filename = args.file[0].replace(".png", ".pnm")
        elif args.i and len(args.i) == 2:
            pnm_filename = args.i[1].replace(".png", ".pnm")

        encoder.encode_pnm(pnm_filename, png_data)

    # save output png also to pnm
    if args.pnm and args.o:
        pnm_filename = args.o.replace(".png", ".pnm")

    # no pnm
    else:
        pnm_filename = None



    # translate image to file
    if args.lc2f:
        output = "".join(instructions)

        if len(args.file) == 2:
            logger.out(output, args.file[1])
        else:
            logger.out(output)
    
    # translate file (+image) to image
    elif args.f2lc:
        if not args.i or not args.o or not os.path.isfile(args.i[0]):
            print("At least one input file and one output file is required.")
            exit(-1)
        if len(args.i) == 2:
            iohandler.save_png(args.o, instructions, pnm_filename, png_data)
        else:
            iohandler.save_png(args.o, instructions, pnm_filename)

    # run interpreter
    else:
        try:
            interpreter.run(instructions, mem, args.test, data, png_data=png_data)
        except BrainxError as e:
            print(e)

    sys.exit(0)

if __name__ == "__main__":
    main()
