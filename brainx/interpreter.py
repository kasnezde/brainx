#!/usr/bin/env python3

""" Program interpreter """

__author__ = "Zdeněk Kasner"

from errors import *
import logger
import string
import sys
import utils


def run(instructions, memory, test, data, png_data):
    # prepare bracket position for loops
    match_opening, match_closing = utils.prepare_brackets(instructions)

    # program counter
    pc = 0  

    # debug log sequence number
    N = 1      

    # output
    output = bytearray()

    # start reading instructions
    while pc < len(instructions):
        c = instructions[pc]

        if c == '>':
            memory.move_right()
            pc += 1

        elif c == '<':
            memory.move_left()
            pc += 1

        elif c == '+':
            memory.inc()
            pc += 1

        elif c == '-':
            memory.dec()
            pc += 1

        elif c == '.':
            byte = memory.peek()
            char = chr(byte).encode('unicode_escape')
            logger.out(chr(byte))
            output.append(byte)
            pc += 1

        elif c == ',':
            byte = data.pop(0) if data else sys.stdin.read(1)
            memory.insert(byte)
            pc += 1

        elif c == '[':
            if memory.peek() == 0:
                pc = match_closing[pc] + 1
            else:
                pc += 1

        elif c == ']': 
            if memory.peek() == 0:
                pc += 1
            else:
                pc = match_opening[pc]

        elif c == "#":
            pc += 1
            logger.log(instructions, memory, output, N, png_data_in=png_data)
            N += 1

        else:
            pc += 1

    # -t option is present
    if test:
        logger.log(instructions, memory, output, N, png_data_in=png_data)

    return memory