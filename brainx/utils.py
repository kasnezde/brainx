#!/usr/bin/env python3

""" Various utility functions """

__author__ = "Zdeněk Kasner"

import errors

# get bytes from input string with escaped byte values
def string_to_bytes(val):
    # overcome difference between tests.py and command line behaviour
    val = val[2:-1] if (val[1] == "'" and val[-1] == "'") else val[1:]

    array = [ord(x) for x in bytes(val, "ascii").decode("unicode_escape")]

    return bytearray(array)



# precompute bracket pairs
def prepare_brackets(commands):
    stack = []
    match_opening = {}

    for i, c in enumerate(commands):
        if c == '[':
            stack.append(i)
        elif c == ']':
            try:
                match_opening[i] = stack.pop()
            except IndexError:
                raise BrainxError("Brackets do not match")

    if stack:
        raise BrainxError("Brackets do not match")

    match_closing = dict((v,k) for k,v in match_opening.items())

    return match_opening, match_closing



# calculate png paeth filter
def paeth(a, b, c):
    p = a + b - c
    pa = abs(p - a)
    pb = abs(p - b)
    pc = abs(p - c)
    
    if pa <= pb and pa <= pc:
        return a
    elif pb <= pc:
        return b
    else:
        return c