#!/usr/bin/env python3

""" Output and logging """

__author__ = "Zdeněk Kasner"

import string


def log_png(dir, png_data):
    png_log_data = ["# RGB ", dir, "\n["]

    for line in png_data:
        png_log_data.append("\n    ")
        png_log_data.append(str(line))
        png_log_data.append(",")
    
    png_log_data.append("\n]\n\n")

    return png_log_data



# dump log file
def log(instructions, memory, output, N, png_data_in = None, png_data_out = None):
    filename = "debug_{:0>2d}.log".format(N)
    log_data = ["# program data\n{}\n\n".format("".join(instructions)),
                "# memory\n{}\n\n".format(memory.get_content().decode("unicode_escape").encode("utf-8")),
                "# memory pointer\n{}\n\n".format(memory.get_pos()),
                "# output\n{}\n\n".format(output.decode("unicode_escape").encode("utf-8"))]

    if png_data_in:
        png_log_data = log_png("input", png_data_in)
        log_data += png_log_data

    if png_data_out:
        png_log_data = log_png("output", png_data_out)
        log_data += png_log_data
    
    with open(filename, "w") as file:
        file.write("".join(log_data))


# write to output
def out(output, filename=None):
    if filename is not None:
        with open(filename, "w") as file:
            file.write(output)
    else:
        print(output, end="", flush=True)