#!/usr/bin/env python3

""" Handling I/O operations """

__author__ = "Zdeněk Kasner"

from errors import *
import traceback
import decoder
import encoder
import os
import sys

symbols = [">", "<", "+", "-", "[", "]", ".", ",", "#"]



# read instructions from console
def load_from_console():
    print("Type code (press Enter to evaluate): ")
    input = sys.stdin.readline().split('!', 1)

    instructions = [instr for instr in input[0] if instr in symbols]
    data = [char for char in input[1]] if len(input) > 1 else []

    return instructions, data



# read instructions from bf file
def load_file(filename):
    if os.path.isfile(filename):
        with open(filename, "r") as file:
            input = file.read().split('!', 1)
    else:
        input = filename.split('!', 1)

    instructions = [instr for instr in input[0] if instr in symbols]
    data = [char for char in input[1]] if len(input) > 1 else []

    return instructions, data



# load instructions from png image
def load_png(filename, instr=True):
    instructions = []
    with open(filename, "rb") as file:
        try:
            if instr:
                instructions, png_data = decoder.load_instructions(file)
            else:
                png_data = decoder.load(file)
        except PNGWrongHeaderError as e:
            sys.stderr.write(str(e))
            sys.exit(4)
        except PNGNotImplementedError as e:
            sys.stderr.write(str(e))
            sys.exit(8)
        except PNGCorruptedError as e:
            sys.stderr.write(str(e))
            sys.exit(2)
        except:
            traceback.print_exc()
            print("Unexpected error:", sys.exc_info()[0])
    if instr:
        return instructions, png_data
    else:
        return png_data




# save instructions to png file
def save_png(filename, instructions, pnm_filename, png_data = None):
    with open(filename, "wb") as file:
        try:
            if png_data:
                encoder.save_braincopter(file, instructions, png_data, pnm_filename)
            else:
                encoder.save_brainloller(file, instructions, pnm_filename)
        except:
            traceback.print_exc()
            print("Unexpected error:", sys.exc_info()[0])