#!/usr/bin/env python3

""" Utils for encoding image formats """

__author__ = "Zdeněk Kasner"

import zlib
from math import sqrt

brainloller_instr = {
    '>' : (255,0,0),
    '<' : (128,0,0),
    '+' : (0,255,0),
    '-' : (0,128,0),
    '.' : (0,0,255),
    ',' : (0,0,128),
    '[' : (255,255,0),
    ']' : (128,128,0),
    'ROTATE_RIGHT': (0,255,255),
    'ROTATE_LEFT' : (0,128,128),
    'nop' : (0,0,0),
}

braincopter_instr = {
    '>' : 0,
    '<' : 1,
    '+' : 2,
    '-' : 3,
    '.' : 4,
    ',' : 5,
    '[' : 6,
    ']' : 7,
    'ROTATE_RIGHT' : 8,
    'ROTATE_LEFT' : 9,
    'nop' : 10,
}



# convert bf instructions in bl png file
def save_brainloller(file, instructions, pnm_file):
    # calculate base line size
    line_size = int(sqrt(len(instructions)))+1

    # split lines by size (omit first instruction)
    lines = [instructions[i:i + line_size] for i in range(1, len(instructions), line_size)]

    # prepend first instruction to the first line
    lines[0].insert(0, instructions[0])

    # fill image bottom with 'nops'
    for i in range(line_size - len(lines[-1]) + 1):
        lines[-1].append('nop')

    # reverse each second line
    for i in range(1, len(lines), 2):
        lines[i].reverse()

    # append right IP rotation at the end of each line
    for i in range(len(lines)):
        lines[i].append('ROTATE_RIGHT')

    # prepend left IP rotation at the end of each line (besides first and last one)
    for i in range(1, len(lines)-1):
        lines[i].insert(0, 'ROTATE_LEFT')

    # save also to pnm file
    if pnm_file:
        encode_pnm(pnm_file, lines)

    # calculate resulting width and length
    width = len(lines[0])
    height = len(lines)

    # convert to RGB values
    pixels = [brainloller_instr[instr] for line in lines for instr in line if instr in brainloller_instr]

    # save to PNG file
    encode(file, pixels, width, height)





# modify pixel to represent selected instruction
def modify(pixel, instr):
    required = braincopter_instr[instr]
    new = list(pixel)

    # change pixel value until it represents selected instruction (without overflow)
    i = 0
    while (-2 * new[0] + 3 * new[1] + new[2]) % 11 != required:
        new[i % 3] = (new[i % 3] + 1) if (new[i % 3] < 255) else (new[i % 3] - 3)
        i+=1

    return tuple(new)



# encode instructions into give png file
def save_braincopter(file, instructions, png_data, pnm_file):
    # add rotating instructions
    for png_line in png_data:
        png_line[-1] = modify(png_line[-1], 'ROTATE_RIGHT')

    for i in range(1, len(png_data)):
        png_data[i][0] = modify(png_data[i][0], 'ROTATE_LEFT')

    # add first instruction to left top corner separately
    png_data[0][0] = modify(png_data[0][0], instructions[0])

    # instruction pointer
    ip = 1

    # add rest of instructions
    for i in range(len(png_data)):
        # line direction =>
        if i % 2 == 0:  
            for j in range(1, len(png_data[i])-1):
                png_data[i][j] = modify(png_data[i][j], instructions[ip] if ip < len(instructions) else 'nop')
                ip += 1

        # line direction <=
        else:            
            for j in range(len(png_data[i])-2, 0, -1):
                png_data[i][j] = modify(png_data[i][j], instructions[ip] if ip < len(instructions) else 'nop')
                ip += 1


    # save also to pnm file
    if pnm_file:
        encode_pnm(pnm_file, png_data)

    width = len(png_data[0])
    height = len(png_data)

    pixels = [pixel for line in png_data for pixel in line]

    # save to png file
    encode(file, pixels, width, height)

    



# encode bytes to png
def encode(file, pixels, width, height):
    # write PNG header
    header = b"\x89PNG\r\n\x1a\n"
    file.write(header)

    # write IHDR chunk
    chunk_data_bytes = width.to_bytes(4, byteorder='big') + height.to_bytes(4, byteorder='big') + bytes([8,2,0,0,0])
    chunk_data = bytearray(chunk_data_bytes)
    write_chunk(file, b"IHDR", chunk_data)

    # write IDAT chunk
    flat_pixels = [pixel for triplet in pixels for pixel in triplet]

    # append filter for each line
    for i in range(0, len(flat_pixels), width * 3 + 1):
        flat_pixels.insert(i, 0)

    chunk_data = bytearray(zlib.compress(bytes(flat_pixels)))
    write_chunk(file, b"IDAT", chunk_data)

    # write IEND chunk
    write_chunk(file, b"IEND", b"")




# wrapper for writing PNG chunk
def write_chunk(file, chunk_type, chunk_data):
    chunk_crc = zlib.crc32(chunk_type + chunk_data).to_bytes(4, byteorder='big')
    chunk_len = (len(chunk_data)).to_bytes(4, byteorder='big')

    file.write(chunk_len)
    file.write(chunk_type)
    file.write(chunk_data)
    file.write(chunk_crc)



# save pixel values to pnm
def encode_pnm(pnm_filename, pnm_data):

    width = len(pnm_data[0])
    height = len(pnm_data)
    pixels = [pixel for line in pnm_data for pixel in line]

    with open(pnm_filename, "wb") as file:
        file.write(bytes("P6\n", "ascii"));
        file.write(bytes("{} {}\n".format(width,height), "ascii"))
        file.write(bytes("255\n", "ascii"))

        for pixel in pixels:
            file.write(bytes(pixel))
