#!/usr/bin/env python3

""" Representation of program memory """

__author__ = "Zdeněk Kasner"

from errors import *


class Memory:
    # create new memory with given state and position
    def __init__(self, state, pos):
        self.memory = state
        self.pointer = pos

        if (self.pointer >= len(self.memory)):
            self.memory += bytearray([0 for i in range(self.pointer - len(self.memory) + 1)])

    # move memory pointer to the right
    def move_right(self):
        self.pointer += 1

        if (self.pointer >= len(self.memory)):
            self.memory.append(0)

    # move memory pointer to the left
    def move_left(self):
        if self.pointer > 0:
            self.pointer -= 1

    # increase value at memory pointer
    def inc(self):
        self.memory[self.pointer] = (self.memory[self.pointer] + 1) % 256

    # decrease value at memory pointer
    def dec(self):
        self.memory[self.pointer] = (self.memory[self.pointer] - 1) % 256

    # get value at memory pointer
    def peek(self):
        return self.memory[self.pointer]

    # insert value at memory pointer
    def insert(self, byte):
        self.memory[self.pointer] = ord(byte)

    # set memory pointer position
    def set_pos(self, pos):
        self.pointer = pos

    # get memory pointer position
    def get_pos(self):
        return self.pointer

    # get all memory content
    def get_content(self):
        return self.memory