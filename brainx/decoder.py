#!/usr/bin/env python3

""" Utils for decoding PNG format """

__author__ = "Zdeněk Kasner"

from errors import *
import utils
import zlib

brainloller_instr = {
    (255,0,0) : '>',
    (128,0,0) : '<',
    (0,255,0) : '+',
    (0,128,0) : '-',
    (0,0,255) : '.',
    (0,0,128) : ',',
    (255,255,0) : '[',
    (128,128,0) : ']',
    (0,255,255) : 'ROTATE_RIGHT',
    (0,128,128) : 'ROTATE_LEFT',
}

braincopter_instr = {
    0  : '>',
    1  : '<',
    2  : '+',
    3  : '-',
    4  : '.',
    5  : ',',
    6  : '[',
    7  : ']',
    8  : 'ROTATE_RIGHT',
    9  : 'ROTATE_LEFT',
    10 : 'nop',
}


# convert instructions in matrix to flat array
def walk_through_instructions(matrix, width, height):
    row = 0
    col = 0

    instructions = []
    dir = 0

    while row >= 0 and row < height and col >= 0 and col < width:
        instr = matrix[row][col]

        if instr == 'ROTATE_RIGHT':
            dir = (dir + 1) % 4
        elif instr == 'ROTATE_LEFT':
            dir = (dir - 1) % 4
        elif instr == 'nop':
            pass
        else:
            instructions.append(instr)

        if dir == 0:        # left
            col += 1
        elif dir == 1:      # down
            row += 1
        elif dir == 2:      # right
            col -= 1
        elif dir == 3:      # up
            row -= 1
        else:
            raise BrainxError("Wrong direction.")

    return instructions



# load png data and bl/bc instructions from png file
def load_instructions(file):
    width, height, png_data = decode(file)
    pixels = reconstruct(width, height, png_data)

    matrix = []

    for row in pixels:
        matrix.append([brainloller_instr[pixel] if pixel in brainloller_instr else 'nop' for pixel in row ])

    # source code is (probably) not brainloller
    nops = sum(line.count('nop') for line in matrix)
    if nops > width and nops > height:
        matrix = []
        for row in pixels:
            matrix.append([braincopter_instr[(-2 * pixel[0] + 3 * pixel[1] + pixel[2]) % 11] for pixel in row ])

    instructions = walk_through_instructions(matrix, width, height)

    return instructions, pixels



# load only png data from png file
def load(file):
    width, height, png_data = decode(file)
    pixels = reconstruct(width, height, png_data)

    return pixels



# reconstruct filtrated png data
def reconstruct(width, height, png_data):
    # get decimal values of bytes
    bytes = [byte for byte in bytearray(zlib.decompress(png_data))]
    
    # count number of bytes for each line
    line_size = width * 3 + 1

    # split bytes into separate lines
    lines = [bytes[i:i + line_size] for i in range(0, len(bytes), line_size)]

    # get filter values for each line
    filters = [line[0] for line in lines]

    # get RGB triplets for each line
    rgb_triplets = [[line[1:][i:i+3] for i in range(0, width*3, 3)] for line in lines]

    for row in range(height):
        filter = filters[row]

        for col in range(width):
            # values of pixels for filters
            x = rgb_triplets[row][col]
            a = rgb_triplets[row][col-1] if col > 0 else [0, 0, 0]
            b = rgb_triplets[row-1][col] if row > 0 else [0, 0, 0]
            c = rgb_triplets[row-1][col-1] if row > 0 and col > 0 else [0, 0, 0]

            # recon(x) = filt(x)
            if filter == 0:
                pass

            # recon(x) = filt(x) + recon(a)
            elif filter == 1:
                rgb_triplets[row][col] = [(x[i] + a[i]) % 256 for i in range(3)]

            # recon(x) = filt(x) + recon(b)
            elif filter == 2:
                rgb_triplets[row][col] = [(x[i] + b[i]) % 256 for i in range(3)]

            # recon(x) = filt(x) + (recon(a) + recon(b)) // 2
            elif filter == 3:
                rgb_triplets[row][col] = [(x[i] + (a[i] + b[i]) // 2) % 256 for i in range(3)]

            # recon(x) = filt(x) + paeth(recon(a), recon(b), recon(c))
            elif filter == 4:
                rgb_triplets[row][col] = [(x[i] + utils.paeth(a[i], b[i], c[i])) % 256 for i in range(3)]

            else:
                raise BrainxError("filter not implemented.")

    # decoded values of pixels
    pixels = []
    for row in rgb_triplets:
        pixels.append([tuple(rgb_triplet) for rgb_triplet in row])

    return pixels



# decode raw png data
def decode(file):
    png_data = b""
    header = file.read(8)

    if header != b"\x89PNG\r\n\x1a\n":
        raise PNGWrongHeaderError("File is not in PNG format.")

    # read all file chunks
    while True:

        # common information for all chunks
        chunk_len = int.from_bytes(file.read(4), byteorder='big')      
        chunk_type = file.read(4)                                     
        chunk_data = file.read(chunk_len)                               
        chunk_crc = int.from_bytes(file.read(4), byteorder='big')


        # header (needed for file decoding)
        if chunk_type == b"IHDR":
            width = int.from_bytes(chunk_data[0:4], byteorder='big')
            height = int.from_bytes(chunk_data[4:8], byteorder='big')
            depth = chunk_data[8]
            color = chunk_data[9]
            compr = chunk_data[10]
            filtr = chunk_data[11]
            interl = chunk_data[12]

            if depth != 8 or color != 2 or compr != 0 or filtr != 0 or interl != 0:
                raise PNGNotImplementedError("Cannot decode this type of PNG file.")

            if chunk_crc != zlib.crc32(chunk_type + chunk_data):
                raise PNGCorruptedError("Bad checksum.")


        # data chunk
        elif chunk_type == b"IDAT":
            png_data += chunk_data

            if chunk_crc != zlib.crc32(chunk_type+chunk_data):
                raise PNGCorruptedError("Bad checksum.")


        # final chunk
        elif chunk_type == b"IEND":
            if chunk_len != 0 or chunk_crc != zlib.crc32(chunk_type+chunk_data):
                raise PNGCorruptedError("Corrupted end of file.")
            break


        # unknown chunk
        # else:
        #     raise PNGNotImplementedError("Wrong type of chunk: {0}.".format(chunk_type))

    return width, height, png_data