#!/usr/bin/env python3

""" Custom error classes """

__author__ = "Zdeněk Kasner"


# generic interpreter error
class BrainxError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return "BrainxError: " + self.value


# png header is corrupted
class PNGWrongHeaderError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return "PNGWrongHeaderError: " + self.value


# type of png format not supported
class PNGNotImplementedError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return "PNGNotImplementedError: " + self.value


# png file is corrupted
class PNGCorruptedError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return "PNGCorruptedError: " + self.value

